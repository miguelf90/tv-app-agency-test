var distance_1 = 0;
var distance_2 = 0;
var images = document.getElementById('carousel-images').children;
var intervalId = 0;
var radios = ['Radio 1', 'Radio 2', 'Radio 3', 'Radio 4', 'Radio 5'];
var current = 2;
var view = 'main';
var twitter = 0;

function nextMove() {
	largura = document.getElementById('carousel-images').offsetWidth;
	small = largura * 0.16;
	big = largura * 0.64;
	for (i=0; i<=3; i++) {
		images[i].style.webkitTransform = "translate("+small+"%, 0)";
	}

	images[4].style.webkitTransform = "translate(-"+big+"%, 0)";

	//cleanUp(0);

}

function previousMove() {

	distance_1 ++;
	distance_2 += 4;

	for (i=1; i<=4; i++) {
		images[i].style.left = -distance_1+'%';
	}

	images[0].style.left = distance_2+'%';

	if (distance_2 == 64) {
		clearInterval(intervalId);
		cleanUp(1);
	}

}

function cleanUp(previous) {

	for (i=0; i<=4; i++) {
		images[i].style.left = 0+'%';
		images[i].style.right = 0+'%';
	}

	if (previous) {
		insertAfter(images[0], images[4]);
		if (current == 4) {
			current = 0;
		}
		else {
			current++;
		}
	}
	else {
		if (current == 0) {
			current = 4;
		}
		else {
			current--;
		}
		document.getElementById('carousel-images').insertBefore(images[4], images[0]);
	}
	intervalId = 0;
	document.getElementById('radio-label-text').innerHTML = radios[current].toString();
}

function insertAfter(newNode, referenceNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}

function player() {
	if (view == 'main') {
		document.getElementById('main').style.display='none';
		document.getElementById('secondary').style.display='block';

		document.getElementById('player-label').innerHTML='VIEW ALL STATIONS';
		document.getElementById('play-label').innerHTML='STOP';
		document.getElementById('play-image').src='images/stop.png';

		view = 'secondary';
	}
	else {
		document.getElementById('secondary').style.display='none';
		document.getElementById('main').style.display='block';

		document.getElementById('player-label').innerHTML='VIEW PLAYER';
		document.getElementById('play-label').innerHTML='PLAY';
		document.getElementById('play-image').src='images/play.png';

		view = 'main';
	}
}

function twitterDisplay() {
	if (twitter==0) {
		moveBoxRight();
	}
	else {
		moveBoxLeft();
	}
}

function moveBoxRight() {
	document.getElementById('twitter').style.webkitTransform = "translate(200px, 0)";
	twitter = 200;
}

function moveBoxLeft() {
	document.getElementById('twitter').style.webkitTransform = "translate(0px, 0)";
	twitter = 0;
}